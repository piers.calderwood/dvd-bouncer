*Dependencies:*
- python3 (3.5+)
- opencv (cv2)
- numpy

Usage:
For video capture

`python capture.py --pipe 1 | ffmpeg -f rawvideo -pixel_format bgr24 -video_size 1280x720 -framerate xx -i - foo.avi`

To display in window

`python capture.py`