'''
To save as video
python capture.py --pipe 1 | ffmpeg -f rawvideo -pixel_format bgr24 -video_size 1280x720 -framerate xx -i - foo.avi
You need to specify the input framerate.
'''
import cv2
#import matplotlib.pyplot as plt
import numpy as np
from time import monotonic
import sys
import argparse

parser = argparse.ArgumentParser(description='Makes your head like the old DVD screensavers')
parser.add_argument('--pipe', dest='pipe', metavar='-p', type=bool, nargs='?', help='If true outputs to stdout for piping. Defaults to False', default=False)
args = parser.parse_args()
use_pipe = args.pipe

start = monotonic()

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
cam = cv2.VideoCapture(0)

# Need better colormaps
colors = [cv2.COLORMAP_AUTUMN, 
          cv2.COLORMAP_COOL, 
          cv2.COLORMAP_HOT, 
          cv2.COLORMAP_OCEAN, 
          cv2.COLORMAP_SPRING, 
          cv2.COLORMAP_SUMMER,
          cv2.COLORMAP_WINTER]
face_dim = (250,250)
frame_size = (720, 1280, 3)

def get_face(color):
    _ret, img = cam.read()
    #img = cv2.flip(img,1)
    img = cv2.resize(img, (img.shape[1]//4, img.shape[0]//4), interpolation = cv2.INTER_AREA)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)
    if len(faces) == 0:
        return None
    (x, y, w, h) = faces[0]
    gray_face = gray[y:y+h, x:x+w]
    mask = np.zeros(gray_face.shape + (3,), np.uint8)
    cv2.circle(mask, (w//2, h//2), max(h//2,w//2), (255, 255, 255), -1)
    #gray_face = cv2.cvtColor(img_face, cv2.COLOR_BGR2GRAY)
    color_face = cv2.applyColorMap(gray_face, color)
    face = color_face & mask
    face = cv2.resize(face, face_dim, interpolation = cv2.INTER_CUBIC)
    face = cv2.GaussianBlur(face, (5,5), 0)
    return face

def bounce(framex, framey, imx, imy, imcenter):
    top_left = imcenter[0] - imx//2, imcenter[1] - imy//2
    bot_right = imcenter[0] + imx//2, imcenter[1] + imy/2
    dx, dy = 1, 1
    if top_left[0] <= 0: # left wall rebound
        dx = -1
    elif bot_right[0] > framex: #right wall rebound
        dx = -1
    if top_left[1] <= 0:
        dy = -1
    elif bot_right[1] > framey:
        dy = -1
    return dx, dy

def main():
    prev = monotonic()
    pos = (frame_size[1]//2,frame_size[0]//2)
    unit_vel = (np.sqrt(2)/2, -np.sqrt(2)/2)
    speed = 200
    color_i = 0 
    face = np.ones_like(face_dim+(3,))
    while prev-start < 10000:
        frame = np.zeros(frame_size, np.uint8)
        now = monotonic()
        dt = min(.1, now - prev)
        prev = now
        new_pos = (int(pos[0] + unit_vel[0] * speed * dt), int(pos[1] + unit_vel[1] * speed * dt))
        dx,dy = bounce(frame_size[1], frame_size[0], face_dim[1], face_dim[0], new_pos)
        if dx < 0 or dy < 0:
            color_i = (color_i + 1) % len(colors)
            unit_vel = (unit_vel[0] * dx, unit_vel[1] * dy)
            new_pos = (int(pos[0] + unit_vel[0] * speed * dt), int(pos[1] + unit_vel[1] * speed * dt))
        pos = new_pos
        #print(colors[color_i])
        next_face = get_face(colors[color_i])
        face = next_face if next_face is not None else face
        frame[pos[1] - face_dim[1]//2: pos[1] + face_dim[1]//2, pos[0] - face_dim[0]//2: pos[0] + face_dim[0]//2, :] = face
        if use_pipe:
            sys.stdout.buffer.write( frame.tostring() )
        else:
            cv2.imshow('Frame', frame)
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

if __name__ == '__main__':
    if not use_pipe:
        print('q to quit')
    main()